import edu.cest.lab01.Cliente;
import edu.cest.lab01.Animal;
import edu.cest.lab01.Pessoa;
public class Aplicacao{
	public static void main(String[] args){
		Cliente client1 = new Cliente();
		client1.nome = "Anderson";
		Animal animal1 = new Animal();
		animal1.nome = "dog";
		Pessoa person1 = new Pessoa();
		person1.nome = "Anderson de novo";
		System.out.println("Cliente: " + client1.nome);
		System.out.println("Animal: " + animal1.nome);
		System.out.println("Pessoa: " + person1.nome);
		}
}
